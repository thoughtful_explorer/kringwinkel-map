#!/usr/bin/python3

import requests
from bs4 import BeautifulSoup
from ast import literal_eval
import simplekml

#Set filename/path for KML file output
kmlfile = "kringwinkel.kml"
#Set KML schema name
kmlschemaname = "kringwinkel"
#Set page URL
pageURL = "https://www.dekringwinkel.be/zoeken/index.html"
#Start a session to be able to save the cookie: No cookie, no service.
session = requests.Session()
session.get(pageURL)
#Set form data with Brussels in the center + 200km
payload = {'item':'brussels','afstand':'200','uren':'0','aanbod':'0'}
#Get the HTML
page = session.get(pageURL,params=payload)
#Soupify the HTML
soup = BeautifulSoup(page.content, 'html.parser')
#Specify the s_map div class to get the XY coordinates
datamap = soup(class_="s_map")

#The string within the HTML is conveniently oriented as a 2D array, so create the array in Python as such using a handy literal_eval function
xyarray = literal_eval(datamap[0]['data-mappins'])

#Initialize kml object
kml = simplekml.Kml()
#Add schema, which is required for a custom address field
schema = kml.newschema(name=kmlschemaname)
schema.newsimplefield(name="address",type="string")

#Iterate through xyarray for each store
for row in xyarray:
    #Pass a dictionary to find_all function for special HTML5 "data-*" attribute, and assign the store ID number (row[2]) as the value
    storeli = soup.find(attrs={"data-id": row[2]})
    #Get the heading and address of the store
    storename = storeli.find(class_="heading").get_text()
    storeaddress = storeli.find(class_="address").contents[1].get_text() + ", " + storeli.find(class_="address").contents[3].get_text()
    #First, create the point name and description in the kml
    point = kml.newpoint(name=storename,description="thrift store")
    #Then, add the custom "SimpleData" address field to kml file with proper hierarchical kml data structure
    point.extendeddata.schemadata.schemaurl = kmlschemaname
    simpledata = point.extendeddata.schemadata.newsimpledata("address",storeaddress)
    #Finally, add coordinates to the feature
    point.coords=[(row[1], row[0])]
#Save the final KML file
kml.save(kmlfile)
