# De Kringwinkel Location Extractor
Extracts Kringwinkel (Belgian second hand / thrift store) locations from the official website, and places them into KML format.

## Dependencies
* Python 3.x
    * Requests module for session functionality
    * Beautiful Soup 4.x (bs4) for scraping
    * Abstract Syntax Trees (ast) module for literal_eval
    * Simplekml for easily building KML files
* Also of course depends on official [De Kringwinkel](https://www.dekringwinkel.be/zoeken/index.html) webpage.
